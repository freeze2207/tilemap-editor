using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System.Collections.Generic;

public class TilemapEditor : EditorWindow
{
    // Two Scriptable object for data storage
    TilemapData tilemapData;        
    OutputTilemap outputTilemapData;

    // name
    TextField tilemap_name_text;

    // Edit mode and selected sprite to be paint, currently in this simple version you can get the mode and selection info in log.
    int edit_mode = 0;          // 0 - none, 1 - Paint, 2 - Delete. Select the same mode will cancel the function
    Sprite selected_sprite;     // Select the same sprite will de-select the sprite

    // Blank sprite for filling avoiding NullReference
    Sprite blank_sprite;

    List<Sprite> sprites = new List<Sprite>();
    
    // new tilemap size
    int map_size = 8;

    Sprite[ , ] new_sprites = new Sprite[8, 8];

    [MenuItem("Tools/TilemapEditor")]
    public static void ShowExample()
    {
        TilemapEditor wnd = GetWindow<TilemapEditor>();
        wnd.titleContent = new GUIContent("TilemapEditor");
    }

    public void OnEnable()
    {
        tilemapData = null;
        outputTilemapData = null;

        blank_sprite = Resources.Load<Sprite>("blank");

        VisualElement root = rootVisualElement;

        VisualElement main_panel = new VisualElement();
        main_panel.style.flexGrow = 1.0f;
        main_panel.style.flexDirection = FlexDirection.Row;
        main_panel.style.borderTopWidth = main_panel.style.borderRightWidth = main_panel.style.borderBottomWidth = main_panel.style.borderLeftWidth = 1.0f;
        main_panel.style.borderTopColor = main_panel.style.borderRightColor = main_panel.style.borderBottomColor = main_panel.style.borderLeftColor = Color.black;
        root.Add(main_panel);

        #region left panel
        VisualElement left_panel = new VisualElement();
        left_panel.style.flexDirection = FlexDirection.Column;
        left_panel.style.flexGrow = 0.25f;
        left_panel.style.borderTopWidth = left_panel.style.borderRightWidth = left_panel.style.borderBottomWidth = left_panel.style.borderLeftWidth = 1.0f;
        left_panel.style.borderTopColor = left_panel.style.borderRightColor = left_panel.style.borderBottomColor = left_panel.style.borderLeftColor = Color.black;
        main_panel.Add(left_panel);

        // Select tilemap data
        ObjectField tilemap_object_field = new ObjectField("Tilemap Data: ");
        tilemap_object_field.objectType = typeof(TilemapData);
        tilemap_object_field.RegisterCallback<ChangeEvent<Object>>(OnTilemapObjectChanged);
        left_panel.Add(tilemap_object_field);

        tilemap_name_text = new TextField("Name: ");

        tilemap_name_text.RegisterCallback<ChangeEvent<string>>(
            (_evt) =>
            {
                if (tilemapData != null) tilemapData.Name = _evt.newValue;
            }
        );
        left_panel.Add(tilemap_name_text);

        // Setting up 3 main function button
        Button paint_button = new Button()
        {
            text = "Paint",
            style =
            {
                width = 80,
                height = 30
            }
        };
        paint_button.RegisterCallback<MouseUpEvent>(OnPaintClickEvent);

        Button delete_button = new Button()
        {
            text = "Delete",
            style =
            {
                width = 80,
                height = 30
            }
        };
        delete_button.RegisterCallback<MouseUpEvent>(OnDeleteClickEvent);

        Button save_button = new Button()
        {
            text = "Save",
            style =
            {
                width = 80,
                height = 30
            }
        };
        save_button.RegisterCallback<MouseUpEvent>(OnSaveClickEvent);

        left_panel.Add(paint_button);
        left_panel.Add(delete_button);
        left_panel.Add(save_button);

        // List view of selectable sprites
        left_panel.Add(new Label("Item List: "));

        ScrollView list_panel = new ScrollView();
        list_panel.showVertical = true;
        sprites = new List<Sprite>(Resources.LoadAll<Sprite>("tileset"));
        for (int i = 0; i < sprites.Count; i++)
        {
            VisualElement image_row_panel = new VisualElement();
            image_row_panel.style.flexDirection = FlexDirection.Row;

            list_panel.Add(image_row_panel);

            Box box = new Box();
            box.style.width = 50;
            box.style.height = 50;
            box.style.marginBottom = 3;
            box.RegisterCallback<MouseUpEvent>(SourceOnClickEvent);
            image_row_panel.Add(box);

            Image image = new Image();
            image.style.flexShrink = 1.0f;
            image.sprite = sprites[i];
            box.Add(image);

            list_panel.Add(new Label(sprites[i].name));       
        }
        left_panel.Add(list_panel);

        #endregion

        #region right panel

        VisualElement right_panel = new VisualElement();
        right_panel.style.flexDirection = FlexDirection.Column;
        right_panel.style.flexGrow = 1f;
        right_panel.style.borderTopWidth = right_panel.style.borderRightWidth = right_panel.style.borderBottomWidth = right_panel.style.borderLeftWidth = 1.0f;
        right_panel.style.borderTopColor = right_panel.style.borderRightColor = right_panel.style.borderBottomColor = right_panel.style.borderLeftColor = Color.black;
        main_panel.Add(right_panel);

        // Select the scriptable object that saves data
        ObjectField output_object_field = new ObjectField("Output Tilemap Data: ");
        output_object_field.objectType = typeof(OutputTilemap);
        output_object_field.RegisterCallback<ChangeEvent<Object>>(OnOutputObjectChanged);
        right_panel.Add(output_object_field);

        right_panel.Add(new Label("New Tilemap Info"));

        for(int i = 0; i < map_size; i ++)
        {
            VisualElement image_row_panel = new VisualElement();
            image_row_panel.style.flexDirection = FlexDirection.Row;
            right_panel.Add(image_row_panel);

            for (int j = 0; j < map_size; j++)
            {
                Box box = new Box();
                box.style.width = 50;
                box.style.height = 50;

                box.style.marginBottom = 3.0f;
                box.style.marginRight = 3.0f;
                box.RegisterCallback<MouseUpEvent>(NewMapOnClickEvent);
                image_row_panel.Add(box);

                Image image = new Image();
                image.style.flexShrink = 1.0f;
                image.sprite = blank_sprite;
                image.name = j + "," + i;       // Make the name contains the row and col info 
                box.Add(image);
            }
        }

        #endregion

        // Setting up the new sprites by filling it with blank sprites
        for (int i = 0; i < map_size; i++)
        {
            for (int j = 0; j < map_size; j++)
            {
                new_sprites[j, i] = blank_sprite;
            }
        }

    }

    private void OnTilemapObjectChanged(ChangeEvent<Object> evt)
    {
        tilemapData = evt.newValue as TilemapData;
        if (tilemapData != null)
        {
            tilemap_name_text.value = tilemapData.Name;         
        }
        else
        {
            tilemap_name_text.value = "";
            sprites.Clear();
        }
    }

    private void OnOutputObjectChanged(ChangeEvent<Object> evt)
    {
        outputTilemapData = evt.newValue as OutputTilemap;
        
    }
 
    // Button click functions
    private void OnPaintClickEvent(MouseUpEvent _event)
    {
        if(edit_mode != 1)
        {
            edit_mode = 1;
        }else
        {
            edit_mode = 0;
        }
        Debug.Log("Edit Mode: " + edit_mode);
       
    }
    private void OnDeleteClickEvent(MouseUpEvent _event)
    {
        if (edit_mode != 2)
        {
            edit_mode = 2;
        }
        else
        {
            edit_mode = 0;
        }

        Debug.Log("Edit Mode: " + edit_mode);
    }

    private void OnSaveClickEvent(MouseUpEvent _event)
    {
        if (outputTilemapData != null)
        {
            outputTilemapData.spriteList = new_sprites;
        }
    }

    // Different event handle for different image click functions
    private void SourceOnClickEvent(MouseUpEvent _event)
    {
        Image image = _event.target as Image;
        if (image != null)
        {
            if(selected_sprite == image.sprite)
            {
                selected_sprite = null;
            }
            else
            {
                selected_sprite = image.sprite;
            }
        }

        Debug.Log(selected_sprite + "selected");
    }

    private void NewMapOnClickEvent(MouseUpEvent _event)
    {
        Image image = _event.target as Image;
        Vector2 position = GetPositionFromName(image.name);

        if(position.x < 0 || position.y < 0 || position.x >= map_size || position.y >= map_size)
        {
            Debug.Log("Invalid position data");
            return;
        }
        // Delete
        if (edit_mode == 2 && image.sprite != blank_sprite)
        {
            image.sprite = blank_sprite;
            new_sprites[(int)position.x, (int)position.y] = blank_sprite;
            
        }
        // Paint
        else if (edit_mode == 1 && selected_sprite != null)
        {
            image.sprite = selected_sprite;
            
            new_sprites[(int)position.x, (int)position.y] = selected_sprite;
        }

    }

    // Retrive the postion data from image name
    private Vector2 GetPositionFromName(string _name)
    {
        return _name.Length == 3 ? new Vector2(_name[0] - 48, _name[2] - 48) : new Vector2(-1, -1);      
    }
}