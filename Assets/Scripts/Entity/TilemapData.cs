﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TilemapData", menuName = "My Assets/Tilemap Data")]
public class TilemapData : ScriptableObject
{
    [Header("Tilemap Information")]
    public string Name;

    public Texture texture;

}
