﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OutputTilemapData", menuName = "My Assets/Output Tilemap Data")]
public class OutputTilemap : ScriptableObject
{
    [Header("Output Tilemap Information")]
    public string Name;

    public Sprite[ , ] spriteList;
}
