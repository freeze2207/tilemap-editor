﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tilemap : MonoBehaviour
{
    public TilemapData tilemap_data;

    public string DisplayName;

    public Texture texture;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Assert(tilemap_data != null, name + "requires a tilemap data ScriptableObject - Tilemap Data");

        DisplayName = tilemap_data.Name;
        texture = tilemap_data.texture;

    }

}
